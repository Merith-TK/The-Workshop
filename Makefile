default:
	@echo "No Default make command configured"
	@echo "Please use either"
	@echo "   - make multimc"
	@echo "or"
	@echo "   - make technic"
	@echo ""
	@echo "MultiMC will make a pack with the updater.exe built in"
	@echo "  along with the git history"
	@echo ""
	@echo "Techic will make a pack that can be used for technic"

multimc:
	git gc
	-rm  -rf update-pack.data/temp
	-rm  -rf update-pack.data/bin
	-rm  -rf update-pack.data/busybox.exe
	7z d ../the-workshop-multimc.zip ./* -r
	7z d ../the-workshop-multimc.zip ./.* -r
	7z a ../the-workshop-multimc.zip ./* -r
	7z a ../the-workshop-multimc.zip ./.git -r
	7z a ../the-workshop-multimc.zip ./.minecraft -r

technic:
	7z d ../the-workshop-technic.zip ./* -r
	7z a ../the-workshop-technic.zip ./.minecraft/* -r

all: multimc technic

