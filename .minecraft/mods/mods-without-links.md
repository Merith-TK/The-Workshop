* Clean F3
	* https://modrinth.com/mod/clean-f3

* Bed Benifits
	* https://modrinth.com/mod/bed-benefits-fabric

* Dawn API
	* https://modrinth.com/mod/dawn

* Disenchanter
	* https://modrinth.com/mod/disenchanter

* Dont burn my stuff
	* https://modrinth.com/mod/DontBurnMyStuff

* extra key binds
	* https://www.curseforge.com/minecraft/mc-mods/origins-extra-keybinds
* musket mod
	* https://www.curseforge.com/minecraft/mc-mods/ewewukeks-musket-mod

* owolib
	* https://modrinth.com/mod/owo-lib

* patchouli
	* https://modrinth.com/mod/patchouli

* sodium
	* https://modrinth.com/mod/sodium

* title-scrolls
	* https://modrinth.com/mod/title-scrolls

* twigs
	* https://modrinth.com/mod/twigs
